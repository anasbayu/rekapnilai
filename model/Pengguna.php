<?php
    require_once("Database.php");

    class Pengguna{
        public function tambah($nama, $pass, $idJenisPengguna){
            $query = "INSERT INTO pengguna (username, password,
                        id_jenis_pengguna)
                        VALUES('$nama', '$pass', '$idJenisPengguna')";

            $db = Database::connect();
            $data = array();
            $err = false;

            $exec = $db->prepare($query);
            $hasil = $exec->execute();

            if(!$hasil){
                $err = true;
            }

            Database::disconnect();
            return $err;
        }

        public function getAll(){
            $query = "SELECT p.id_pengguna, p.username, j.nama_jenis_pengguna
                        FROM pengguna p
                        JOIN jenis_pengguna j
                        ON p.id_jenis_pengguna = j.id_jenis_pengguna";

            $db = Database::connect();
            $data = array();
            $err = false;

            if($db->query($query)->rowCount() != 0)
            {
                array_push($data, $err);
                $tmp = [];
                foreach($db->query($query) as $pengguna)
                {
                    array_push($tmp, $pengguna);
                }
                array_push($data, $tmp);
            }else{
                $err = true;
                array_push($data, $err);
            }

            Database::disconnect();
            return $data;
        }

        public function getAllJenis(){
            $query = "SELECT * FROM jenis_pengguna";

            $db = Database::connect();
            $data = array();
            $err = false;

            if($db->query($query)->rowCount() != 0)
            {
                array_push($data, $err);
                $tmp = [];
                foreach($db->query($query) as $pengguna)
                {
                    array_push($tmp, $pengguna);
                }
                array_push($data, $tmp);
            }else{
                $err = true;
                array_push($data, $err);
            }

            Database::disconnect();
            return $data;
        }

        public function hapus($id){
            $query = "DELETE FROM pengguna WHERE id_pengguna = '$id'";

            $db = Database::connect();
            $exec = $db->prepare($query);
            $hasil = $exec->execute();

            return $hasil;
        }

        public function login($username, $pass){
            $query = "SELECT p.username, j.nama_jenis_pengguna
                        FROM pengguna p JOIN jenis_pengguna j
                        ON p.id_jenis_pengguna = j.id_jenis_pengguna
                        WHERE username='$username' AND password='$pass'";
            $db = Database::connect();

            $data = array();
            $err = false;

            if($db->query($query)->rowCount() <= 0){
                $err = true;
                array_push($data, $err);
            }else{
                foreach ($db->query($query) as $tmpData) {
                    array_push($data, $err);
                    array_push($data, $tmpData["username"]);
                    array_push($data, $tmpData["nama_jenis_pengguna"]);
                }
            }

            Database::disconnect();
            return $data;
        }

        public function ubahPass($lama, $baru, $username){
            $query = "UPDATE pengguna SET password = '$baru'
                        WHERE username = '$username' AND password = '$lama'";

            $db = Database::connect();
            $data = array();
            $err = false;

            $exec = $db->prepare($query);
            $exec->execute();

            if($exec->rowCount() <= 0){
                $err = true;
            }

            Database::disconnect();
            return $err;
        }
    }
?>
