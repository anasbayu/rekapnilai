<?php

class Database {
    private static $hostname = 'localhost';
    private static $username = 'root';
    private static $password = '';
    private static $database = 'rekap_nilai_db';
    private static $conn = null;

    public function __construct(){
        die('Init function is not allowed');
    }

    public static function connect(){
        if (self::$conn == null){
            try{
                self::$conn = new PDO('mysql:host=' . self::$hostname . ';dbname=' . self::$database . ";", self::$username, self::$password);
            }catch (Exception $e){
                die($e->getMessage());
            }

            return self::$conn;
        }
    }

    public static function disconnect(){
        self::$conn = null;
    }
}
