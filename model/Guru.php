<?php
    require_once("Database.php");

    class Guru{
        public function tambah($nama, $nip, $jk){
            $query = "INSERT INTO guru (nama_guru, nip, id_jenis_kelamin)
                        VALUES('$nama', '$nip', '$jk')";

            $query_user = "INSERT INTO pengguna (username, password,
                        id_jenis_pengguna)
                        VALUES('$nip', 'guru12', '2')";

            $db = Database::connect();
            $data = array();
            $err = false;

            $exec = $db->prepare($query);
            $exec_user = $db->prepare($query_user);
            $hasil = $exec->execute();
            $hasil_user = $exec_user->execute();

            if(!$hasil && $hasil_user){
                $err = true;
            }

            Database::disconnect();
            return $err;
        }

        public function getAll(){
            $query = "SELECT g.id_guru, g.nama_guru, g.nip, jk.nama_jenis_kelamin
                        FROM guru g JOIN jenis_kelamin jk
                        ON g.id_jenis_kelamin = jk.id_jenis_kelamin";

            $db = Database::connect();
            $data = array();
            $err = false;

            if($db->query($query)->rowCount() != 0)
            {
                array_push($data, $err);
                $tmp = [];
                foreach($db->query($query) as $guru)
                {
                    array_push($tmp, $guru);
                }
                array_push($data, $tmp);
            }else{
                $err = true;
                array_push($data, $err);
            }

            Database::disconnect();
            return $data;
        }

        public function hapus($idGuru){
            $query = "DELETE FROM guru WHERE id_guru = '$idGuru'";

            $db = Database::connect();
            $exec = $db->prepare($query);
            $hasil = $exec->execute();

            return $hasil;
        }

        public function tambahRelasiMapelGuru($idGuru, $idMapel){
            $query = "INSERT INTO trans_guru_mapel (id_guru, id_mapel)
                        VALUES('$idGuru', '$idMapel')";

            $db = Database::connect();
            $data = array();
            $err = false;

            $exec = $db->prepare($query);
            $hasil = $exec->execute();

            if(!$hasil){
                $err = true;
            }

            Database::disconnect();
            return $err;
        }
    }
?>
