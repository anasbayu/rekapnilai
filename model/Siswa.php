<?php
    require_once("Database.php");

    class Siswa{
        public function tambah($nama, $nis, $idKelas, $idKelamin){
            $query = "INSERT INTO siswa (nama_siswa, nis,
                        id_kelas, id_jenis_kelamin)
                        VALUES('$nama', '$nis', '$idKelas', '$idKelamin')";

            $db = Database::connect();
            $data = array();
            $err = false;

            $exec = $db->prepare($query);
            $hasil = $exec->execute();

            if(!$hasil){
                $err = true;
            }

            Database::disconnect();
            return $err;
        }

        public function getAll(){
            $query = "SELECT s.id_siswa, s.nama_siswa, s.nis,
                        j.nama_jenis_kelamin, k.nama_kelas
                        FROM siswa s
                        JOIN jenis_kelamin j
                        ON s.id_jenis_kelamin = j.id_jenis_kelamin
                        JOIN kelas k
                        ON s.id_kelas = k.id_kelas";

            $db = Database::connect();
            $data = array();
            $err = false;

            if($db->query($query)->rowCount() != 0)
            {
                array_push($data, $err);
                $tmp = [];
                foreach($db->query($query) as $siswa)
                {
                    array_push($tmp, $siswa);
                }
                array_push($data, $tmp);
            }else{
                $err = true;
                array_push($data, $err);
            }

            Database::disconnect();
            return $data;
        }

        public function hapus($id){
            $query = "DELETE FROM siswa WHERE id_siswa = '$id'";

            $db = Database::connect();
            $exec = $db->prepare($query);
            $hasil = $exec->execute();

            return $hasil;
        }

    }
?>
