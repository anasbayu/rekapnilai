<?php
    require_once("Database.php");

    class Mapel{
        public function tambah($nama){
            $query = "INSERT INTO mapel (nama_mapel)
                        VALUES('$nama')";

            $db = Database::connect();
            $data = array();
            $err = false;

            $exec = $db->prepare($query);
            $hasil = $exec->execute();

            if(!$hasil){
                $err = true;
            }

            Database::disconnect();
            return $err;
        }

        public function getAll(){
            $query = "SELECT * FROM mapel";

            $db = Database::connect();
            $data = array();
            $err = false;

            if($db->query($query)->rowCount() != 0)
            {
                array_push($data, $err);
                $tmp = [];
                foreach($db->query($query) as $mapel)
                {
                    array_push($tmp, $mapel);
                }
                array_push($data, $tmp);
            }else{
                $err = true;
                array_push($data, $err);
            }

            Database::disconnect();
            return $data;
        }

        public function hapus($id){
            $query = "DELETE FROM mapel WHERE id_mapel = '$id'";

            $db = Database::connect();
            $exec = $db->prepare($query);
            $hasil = $exec->execute();

            return $hasil;
        }

        public function getAllRelasiGuru(){
            $query = "SELECT t.id_trans_guru_mapel, g.nama_guru, m.nama_mapel
                        FROM trans_guru_mapel t
                        JOIN guru g ON t.id_guru = g.id_guru
                        JOIN mapel m ON m.id_mapel = t.id_mapel";

            $db = Database::connect();
            $data = array();
            $err = false;

            if($db->query($query)->rowCount() != 0)
            {
                array_push($data, $err);
                $tmp = [];
                foreach($db->query($query) as $mapel)
                {
                    array_push($tmp, $mapel);
                }
                array_push($data, $tmp);
            }else{
                $err = true;
                array_push($data, $err);
            }

            Database::disconnect();
            return $data;
        }
    }
?>
