<?php
    require_once("Database.php");

    class Kelas{
        public function tambah($nama, $idGuru){
            $query = "INSERT INTO kelas (nama_kelas, id_guru)
                        VALUES('$nama', '$idGuru')";

            $db = Database::connect();
            $data = array();
            $err = false;

            $exec = $db->prepare($query);
            $hasil = $exec->execute();

            if(!$hasil){
                $err = true;
            }

            Database::disconnect();
            return $err;
        }

        public function getAll(){
            $query = "SELECT k.id_kelas, k.nama_kelas, g.nama_guru
                        FROM kelas k JOIN guru g
                        ON k.id_guru = g.id_guru";

            $db = Database::connect();
            $data = array();
            $err = false;

            if($db->query($query)->rowCount() != 0)
            {
                array_push($data, $err);
                $tmp = [];
                foreach($db->query($query) as $kelas)
                {
                    array_push($tmp, $kelas);
                }
                array_push($data, $tmp);
            }else{
                $err = true;
                array_push($data, $err);
            }

            Database::disconnect();
            return $data;
        }

        public function hapus($id){
            $query = "DELETE FROM kelas WHERE id_kelas = '$id'";

            $db = Database::connect();
            $exec = $db->prepare($query);
            $hasil = $exec->execute();

            return $hasil;
        }
    }
?>
