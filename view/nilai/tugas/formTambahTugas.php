<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Data Tugas
            <br>
            <small>Data master Tugas</small>
        </h1>
    </section>
    <section class="content">
        <?php
            error_reporting(0);
            if($_GET["err"]){?>
                <div class="callout callout-danger lead">
                    <p>Terjadi kesalahan, silahkan ulangi kembali</p>
                </div>
        <?php
            }
            if($_GET["sukses"]){?>
                <div class="callout callout-success lead">
                    <p>Berhasil Menambahkan Data Tugas</p>
                </div>
        <?php
            }
        ?>
        <div class="box box-danger">
            <div class="box-header with-border">
                <h3 class="box-title">Tambah Data Tugas</h3>
            </div>
            <div class="box-body">
                <form role="form" method="post" action="<?php route('tambahTugas')?>">
                    <div class="form-group">
                        <label>Nama Tugas</label>
                        <input type="text" class="form-control" name="tugas" placeholder="Nama Tugas">
                    </div>
                    <input type="hidden" class="form-control" name="mode" value="tambah">
                    <div class="form-group">
                        <button type="submit" class="btn btn-danger">Tambah</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
</div>
