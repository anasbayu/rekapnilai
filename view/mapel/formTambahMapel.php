<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Data Mapel
            <br>
            <small>Data master Mapel sekolah</small>
        </h1>
    </section>
    <section class="content">
        <?php
            error_reporting(0);
            if($_GET["err"]){?>
                <div class="callout callout-danger lead">
                    <p>Terjadi kesalahan, silahkan ulangi kembali</p>
                </div>
        <?php
            }
            if($_GET["sukses"]){?>
                <div class="callout callout-success lead">
                    <p>Berhasil Menambahkan Data Mapel</p>
                </div>
        <?php
            }
        ?>
        <div class="box box-danger">
            <div class="box-header with-border">
                <h3 class="box-title">Tambah Data Mapel</h3>
            </div>
            <div class="box-body">
                <form role="form" method="post" action="<?php route('tambahMapel')?>">
                    <div class="form-group">
                        <label>Nama Pelajaran</label>
                        <input type="text" class="form-control" name="nama" placeholder="Nama Mata Pelajaran">
                    </div>
                    <input type="hidden" class="form-control" name="mode" value="tambah">
                    <div class="form-group">
                        <button type="submit" class="btn btn-danger">Tambah</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
</div>
