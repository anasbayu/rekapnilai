<?php
    include_once("../route.php");
    session_start();

    // Kalau udah login, set username dan level.
    // Kalau belum redirect ke form login.
    if(isset($_SESSION['username'])) {
        $username = $_SESSION['username'];
        $level = $_SESSION['level'];
    }else{
        route("formLogin");
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <title>Sistem Rekapitulasi Nilai</title>
        <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
        <link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
        <link rel="stylesheet" href="../dist/css/skins/skin-red.min.css">
        <link rel="stylesheet" href="../plugins/select2/select2.min.css">
        <link rel="stylesheet" href="../plugins/datepicker/datepicker3.css">
        <link rel="stylesheet" href="../plugins/datatables/dataTables.bootstrap.css">
        <link rel="stylesheet" href="../plugins/iCheck/all.css">
        <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
        <link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
        <link rel="stylesheet" href="../dist/css/skins/_all-skins.min.css">
        <script src="../plugins/iCheck/icheck.min.js"></script>
        <script src="../plugins/jQuery/jquery-2.2.3.min.js"></script>
        <script src="../bootstrap/js/bootstrap.min.js"></script>
        <script src="../dist/js/app.min.js"></script>
        <script src="../plugins/datepicker/bootstrap-datepicker.js"></script>
        <script src="../plugins/select2/select2.full.min.js"></script>
        <script src="../plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="../plugins/datatables/dataTables.bootstrap.min.js"></script>
        <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js" type="text/javascript"></script> -->
    </head>

    <body class="hold-transition skin-red sidebar-mini">
        <div class="example-modal">
            <div class="modal modal-danger">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <h4 class="modal-title">Notifikasi</h4>
                        </div>
                        <div class="modal-body">
                            <p>Apakah Anda yakin ingin keluar dari sistem?</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Batal</button>
                            <a href="<?php route('logout')?>"><button type="button" class="btn btn-outline" id="keluar">Keluar</button></a>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->
        </div>
        <div class="wrapper">
            <header class="main-header">
                <a href="<?php route('dashboard') ?>" class="logo">
                    <span class="logo-mini"><b>S</b>RN</span>
                    <span class="logo-lg"><b>Sistem</b>Rekapitulasi</span>
                </a>

                <nav class="navbar navbar-static-top" role="navigation">
                    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                        <span class="sr-only">Toggle navigation</span>
                    </a>
                    <!-- Navbar Right Menu -->
                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">
                        </ul>
                    </div>
                </nav>
            </header>
            <aside class="main-sidebar">
                <section class="sidebar">
                    <div class="user-panel">
                        <div class="pull-left image">
                            <img src="../dist/img/tutwuri.png" class="img-circle" alt="Profil">
                        </div>
                        <div class="pull-left info">
                            <p>Hallo <?php echo $username;?>!</p>
                            <i><?php echo $level;?></i>
                        </div>
                    </div>

                    <!-- Ini menu samping -->
                    <ul class="sidebar-menu">
                        <li>
                            <a href="<?php route('formUbahPassword');?>"><i class="fa fa-key "></i> <span>Ubah Password</span></a></li>
                        <li>
                            <a data-toggle='modal' data-target='.modal-danger'><i class="fa fa-sign-out keluar"></i> <span>Keluar Sistem</span></a>
                        </li>

                        <?php if($level == "admin"){?>
                            <!-- GURU  -->
                            <li class="treeview">
                                <a href="#"><i class="fa fa-file-text-o"></i> <span>Master Guru</span>
                                    <span class="pull-right-container">
                                        <i class="fa fa-angle-left pull-right"></i>
                                    </span>
                                </a>
                                <ul style="display: none;" class="treeview-menu">
                                    <li><a href="<?php route('formTambahGuru')?>"><i class="fa fa-key "></i> <span>Tambah Data Guru</span></a></li>
                                    <li><a href="<?php route('listGuru')?>"><i class="fa fa-key "></i> <span>Kelola Data Guru</span></a></li>
                                </ul>
                            </li>

                            <!-- MAPEL -->
                            <li class="treeview">
                                <a href="#"><i class="fa fa-file-text-o"></i> <span>Master Mata Pelajaran</span>
                                    <span class="pull-right-container">
                                        <i class="fa fa-angle-left pull-right"></i>
                                    </span>
                                </a>
                                <ul style="display: none;" class="treeview-menu">
                                    <li><a href="<?php route('formTambahMapel')?>"><i class="fa fa-key "></i> <span>Tambah Data Mapel</span></a></li>
                                    <li><a href="<?php route('listMapel')?>"><i class="fa fa-key "></i> <span>Kelola Data Mapel</span></a></li>
                                </ul>
                            </li>

                            <!-- KELAS  -->
                            <li class="treeview">
                                <a href="#"><i class="fa fa-file-text-o"></i> <span>Master Kelas</span>
                                    <span class="pull-right-container">
                                        <i class="fa fa-angle-left pull-right"></i>
                                    </span>
                                </a>
                                <ul style="display: none;" class="treeview-menu">
                                    <li><a href="<?php route('formTambahKelas')?>"><i class="fa fa-key "></i> <span>Tambah Data Kelas</span></a></li>
                                    <li><a href="<?php route('listKelas')?>"><i class="fa fa-key "></i> <span>Kelola Data Kelas</span></a></li>
                                </ul>
                            </li>

                            <!-- PENGGUNA  -->
                            <li class="treeview">
                                <a href="#"><i class="fa fa-file-text-o"></i> <span>Master Pengguna</span>
                                    <span class="pull-right-container">
                                        <i class="fa fa-angle-left pull-right"></i>
                                    </span>
                                </a>
                                <ul style="display: none;" class="treeview-menu">
                                    <li><a href="<?php route('formTambahPengguna')?>"><i class="fa fa-key "></i> <span>Tambah Data Pengguna</span></a></li>
                                    <li><a href="<?php route('listPengguna')?>"><i class="fa fa-key "></i> <span>Kelola Data Pengguna</span></a></li>
                                </ul>
                            </li>

                            <!-- Nilai  -->
                            <li class="treeview">
                                <a href="#"><i class="fa fa-file-text-o"></i> <span>Master Nilai</span>
                                    <span class="pull-right-container">
                                        <i class="fa fa-angle-left pull-right"></i>
                                    </span>
                                </a>
                                <ul style="display: none;" class="treeview-menu">
                                    <li><a href=""><i class="fa fa-file-text-o "></i> <span>Tugas</span></a>
                                        <ul style="display: none;" class="treeview-menu">
                                            <li><a href="<?php route('formTambahTugas')?>"><i class="fa fa-key "></i> <span>Tambah Data Tugas</span></a></li>
                                            <li><a href="<?php route('listTugas')?>"><i class="fa fa-key "></i> <span>Kelola Data Tugas</span></a></li>
                                        </ul>
                                    </li>
                                    <li><a href=""><i class="fa fa-file-text-o "></i> <span>Ulangan Harian</span></a>
                                        <ul style="display: none;" class="treeview-menu">
                                            <li><a href="<?php route('formTambahUH')?>"><i class="fa fa-key "></i> <span>Tambah Data UH</span></a></li>
                                            <li><a href="<?php route('listUH')?>"><i class="fa fa-key "></i> <span>Kelola Data UH</span></a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>

                            <!-- SISWA  -->
                            <li class="treeview">
                                <a href="#"><i class="fa fa-file-text-o"></i> <span>Master Siswa</span>
                                    <span class="pull-right-container">
                                        <i class="fa fa-angle-left pull-right"></i>
                                    </span>
                                </a>
                                <ul style="display: none;" class="treeview-menu">
                                    <li><a href="<?php route('formTambahSiswa')?>"><i class="fa fa-key "></i> <span>Tambah Data Siswa</span></a></li>
                                    <li><a href="<?php route('listSiswa')?>"><i class="fa fa-key "></i> <span>Kelola Data Siswa</span></a></li>
                                </ul>
                            </li>

                            <!-- RELASI  -->
                            <li class="treeview">
                                <a href="#"><i class="fa fa-file-text-o"></i> <span>Data Relasi</span>
                                    <span class="pull-right-container">
                                        <i class="fa fa-angle-left pull-right"></i>
                                    </span>
                                </a>
                                <ul style="display: none;" class="treeview-menu">
                                    <li><a href="<?php route('formRelasiMapelGuru')?>"><i class="fa fa-key "></i> <span>Relasi Guru &  Mapel</span></a></li>
                                    <li><a href="<?php route('daftarRelasiMapelGuru')?>"><i class="fa fa-key "></i> <span>Daftar Relasi Guru &  Mapel</span></a></li>
                                </ul>
                            </li>
                        <?php } ?>

                       <!--  ============this is right of teacher================== -->
                       <?php if($level == "guru"){?>
                            <li class="treeview">
                                <a href="#"><i class="fa fa-file-text-o"></i> <span>Ulangan Harian</span>
                                    <span class="pull-right-container">
                                        <i class="fa fa-angle-left pull-right"></i>
                                    </span>
                                </a>
                                <ul style="display: none;" class="treeview-menu">
                                    <li><a href="#"><i class="fa fa-key "></i> <span>Tambah Nilai</span></a></li>
                                    <li><a href="#"><i class="fa fa-key "></i> <span>Kelola Data Nilai</span></a></li>
                                </ul>
                            </li>
                            <li class="treeview">
                                <a href="#"><i class="fa fa-file-text-o"></i> <span>Tugas</span>
                                    <span class="pull-right-container">
                                        <i class="fa fa-angle-left pull-right"></i>
                                    </span>
                                </a>
                                <ul style="display: none;" class="treeview-menu">
                                    <li><a href="<?php route('formTambahNilaiTgs')?>"><i class="fa fa-key "></i> <span>Tambah Nilai</span></a></li>
                                    <li><a href="#"><i class="fa fa-key "></i> <span>Kelola Data Nilai</span></a></li>
                                </ul>
                            </li>
                            <li class="treeview">
                                <a href="#"><i class="fa fa-file-text-o"></i> <span>UTS</span>
                                    <span class="pull-right-container">
                                        <i class="fa fa-angle-left pull-right"></i>
                                    </span>
                                </a>
                                <ul style="display: none;" class="treeview-menu">
                                    <li><a href="#"><i class="fa fa-key "></i> <span>Tambah Nilai</span></a></li>
                                    <li><a href="#"><i class="fa fa-key "></i> <span>Kelola Data Nilai</span></a></li>
                                </ul>
                            </li>
                            <li class="treeview">
                                <a href="#"><i class="fa fa-file-text-o"></i> <span>UAS</span>
                                    <span class="pull-right-container">
                                        <i class="fa fa-angle-left pull-right"></i>
                                    </span>
                                </a>
                                <ul style="display: none;" class="treeview-menu">
                                    <li><a href="#"><i class="fa fa-key "></i> <span>Tambah Nilai</span></a></li>
                                    <li><a href="#"><i class="fa fa-key "></i> <span>Kelola Data Nilai</span></a></li>
                                </ul>
                            </li>
                       <?php }?>
                    </ul>
                </section>

                <!-- Script untuk menampilkan modal keluar -->
                <script type="text/javascript">
                    $(".keluar").on("click", function(){
                        var form=$(this).parent();
                        $("#keluar").click(function(){
                            form.trigger("submit");
                        });
                    });
                </script>
            </aside>
            <?php
                switch ($_GET['halaman']) {
                    case 'dashboard':
                        include 'dashboard.php';
                        break;
                    case 'formUbahPassword':
                        include 'formUbahPassword.php';
                        break;

                    // Guru.
                    case 'tambahGuru':
                        include 'guru/formTambahGuru.php';
                        break;
                    case 'kelolaGuru':
                        include 'guru/daftarGuru.php';
                        break;

                    // Mapel.
                    case 'tambahMapel':
                        include 'mapel/formTambahMapel.php';
                        break;
                    case 'kelolaMapel':
                        include 'mapel/daftarMapel.php';
                        break;
                    default:

                    // Kelas.
                    case 'tambahKelas':
                        include 'kelas/formTambahKelas.php';
                        break;
                    case 'kelolaKelas':
                        include 'kelas/daftarKelas.php';
                        break;

                    // Pengguna.
                    case 'tambahPengguna':
                        include 'pengguna/formTambahPengguna.php';
                        break;
                    case 'kelolaPengguna':
                        include 'pengguna/daftarPengguna.php';
                        break;
                    //Dafta Tugas
                    case 'tambahTugas':
                        include 'nilai/tugas/formTambahTugas.php';
                        break;
                    case 'kelolaTugas':
                        include 'nilai/tugas/daftarTugas.php';
                        break;
                    //Daftar Ulangan
                    case 'tambahUH':
                        include 'nilai/UH/formTambahUH.php';
                        break;
                    case 'kelolaUH':
                        include 'nilai/UH/daftarUH.php';
                        break;
                    //Nilai Tugas
                    case 'tambahNilaiTgs':
                        include 'nilai/tugas/formTambahNilai.php';
                        break;
                    // Siswa.
                    case 'tambahSiswa':
                        include 'siswa/formTambahSiswa.php';
                        break;
                    case 'kelolaSiswa':
                        include 'siswa/daftarSiswa.php';
                        break;

                    // Relasi.
                    case 'formRelasiMapelGuru':
                        include 'relasi/formRelasiMapelGuru.php';
                        break;
                    case 'daftarRelasiMapelGuru':
                        include 'relasi/daftarRelasiMapelGuru.php';
                        break;

                    default:
                        // include 'halaman/index.php';
                        break;
                }
            ?>
            <footer class="main-footer">
                <!-- <strong>Copyright &copy; 2017 <a href="#">KusumaTech</a>.</strong> -->
            </footer>
        </div>
    </body>
</html>
