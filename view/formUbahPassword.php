<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Ubah Password
            <br>
            <small>Ubah Password Pengguna</small>
        </h1>
    </section>
    <section class="content">
        <?php
            error_reporting(0);
            if($_GET["err"] == "true"){?>
                <div class="callout callout-danger lead">
                    <p>Terjadi kesalahan, silahkan ulangi kembali</p>
                </div>
        <?php
            }
            else if($_GET["err"] == "pass"){?>
                <div class="callout callout-danger lead">
                    <p>Password harus sama, silahkan ulangi kembali</p>
                </div>
        <?php
            }
            if($_GET["sukses"]){?>
                <div class="callout callout-success lead">
                    <p>Berhasil Merubah Password</p>
                </div>
        <?php
            }
        ?>
        <div class="box box-danger">
            <div class="box-header with-border">
                <h3 class="box-title">Ubah Password</h3>
            </div>
            <div class="box-body">
                <form role="form" method="post" action="<?php route('ubahPassword')?>">
                    <div class="form-group">
                        <label>Password Lama</label>
                        <input type="password" class="form-control" name="pass_lama" placeholder="Masukkan Password Lama Anda" required>
                    </div>

                    <div class="form-group">
                        <label>Password Baru</label>
                        <input type="password" class="form-control" name="pass_baru" placeholder="Password Baru" required>
                    </div>

                    <div class="form-group">
                        <label>Konfirmasi Password</label>
                        <input type="password" class="form-control" name="pass_konfirm" placeholder="Konfirmasi Password" required>
                    </div>

                    <input type="hidden" class="form-control" name="mode" value="ubahPassword">

                    <div class="form-group">
                        <button type="submit" class="btn btn-danger">Tambah</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
</div>
