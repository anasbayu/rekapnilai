<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Data Kelas
            <br>
            <small>Data master Kelas</small>
        </h1>
    </section>
    <section class="content">
        <?php
            error_reporting(0);
            if($_GET["err"]){?>
                <div class="callout callout-danger lead">
                    <p>Terjadi kesalahan, silahkan ulangi kembali</p>
                </div>
        <?php
            }
            if($_GET["sukses"]){?>
                <div class="callout callout-success lead">
                    <p>Berhasil Menambahkan Data Kelas</p>
                </div>
        <?php
            }
        ?>
        <div class="box box-danger">
            <div class="box-header with-border">
                <h3 class="box-title">Tambah Data Kelas</h3>
            </div>
            <div class="box-body">
                <form role="form" method="post" action="<?php route('tambahKelas')?>">
                    <div class="form-group">
                        <label>Nama Kelas</label>
                        <input type="text" class="form-control" name="nama_kelas" placeholder="Nama Kelas" required>
                    </div>

                    <div class="form-group">
                        <label>Wali Kelas</label>
                        <select class="form-control" id="wali" name="id_guru" required>
                        </select>
                    </div>

                    <input type="hidden" class="form-control" name="mode" value="tambah">
                    <div class="form-group">
                        <button type="submit" class="btn btn-danger">Tambah</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $.get("<?php route("ambilSemuaGuru");?>"
            , function(raw){
                var data = $.parseJSON(raw);
                var err = data[0];
                var rawData = data[1];
                if(!err){
                    for(var i = 0; i < rawData.length; i++){
                        var tmpData = rawData[i];
                        $('#wali').append("" +
                            "<option value=" + tmpData['id_guru'] +">" +
                                tmpData['nama_guru'] +
                            "</option>" +
                        "");
                    }
                }
        });
    });
</script>
