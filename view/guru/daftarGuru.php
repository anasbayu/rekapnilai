<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Data Guru
            <br>
            <small>Data master guru sekolah</small>
        </h1>
    </section>
    <section class="content">
        <?php
            error_reporting(0);
            if($_GET["err"]){?>
                <div class="callout callout-danger lead">
                    <p>Terjadi kesalahan, silahkan ulangi kembali</p>
                </div>
        <?php
            }
            if($_GET["sukses"]){?>
                <div class="callout callout-success lead">
                    <p>Berhasil Merubah Data Guru</p>
                </div>
        <?php
            }
        ?>
        <div class="box box-danger">
            <div class="box-header with-border">
                <h3 class="box-title">Kelola Data Guru</h3>
            </div>
            <div class="box-body">
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama</th>
                            <th>NIP</th>
                            <th>Jenis Kelamin</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody id="tabel">
                        <script type="text/javascript">
                            $(document).ready(function(){
                                $.get("<?php route("ambilSemuaGuru");?>"
                                    , function(raw){
                                        var data = $.parseJSON(raw);
                                        var err = data[0];
                                        var rawData = data[1];
                                        if(!err){
                                            for(var i = 0; i < rawData.length; i++){
                                                var tmpData = rawData[i];
                                                $('#tabel').append("" +
                                                    "<tr>" +
                                                        "<td>" + (i+1) +"</td>" +
                                                        "<td>" + tmpData['nama_guru'] +"</td>" +
                                                        "<td>" + tmpData['nip'] + "</td>" +
                                                        "<td>" + tmpData['nama_jenis_kelamin']+ "</td>" +
                                                        "<td><a  href='#' class='btn btn-xs btn-info'><span class='fa fa-pencil'> </span></a> | <a class='btn btn-xs btn-danger' href='<?php route("hapusGuru")?>" + tmpData['id_guru'] + "'><span class='fa fa-trash'> </span></a>" + "</td>" +
                                                    "</tr>" +
                                                "");
                                            }
                                        }
                                });
                            });
                        </script>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
</div>
