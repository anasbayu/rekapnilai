<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Data Siswa
            <br>
            <small>Data master Siswa</small>
        </h1>
    </section>
    <section class="content">
        <?php
            error_reporting(0);
            if($_GET["err"]){?>
                <div class="callout callout-danger lead">
                    <p>Terjadi kesalahan, silahkan ulangi kembali</p>
                </div>
        <?php
            }
            if($_GET["sukses"]){?>
                <div class="callout callout-success lead">
                    <p>Berhasil Menambahkan Data Siswa</p>
                </div>
        <?php
            }
        ?>
        <div class="box box-danger">
            <div class="box-header with-border">
                <h3 class="box-title">Tambah Data Siswa</h3>
            </div>
            <div class="box-body">
                <form role="form" method="post" action="<?php route('tambahSiswa')?>">
                    <div class="form-group">
                        <label>Nama Siswa</label>
                        <input type="text" class="form-control" name="nama_siswa" placeholder="Nama Siswa" required>
                    </div>

                    <div class="form-group">
                        <label>NIS</label>
                        <input type="text" class="form-control" name="nis" placeholder="NIS" required>
                    </div>

                    <div class="form-group">
                        <label>Jenis Kelamin</label>
                        <div class="radio">
                            <label>
                                <input type="radio" name="kelamin" id="optionsRadios1" value="1">
                                Laki-laki
                            </label>
                        </div>
                        <div class="radio">
                            <label>
                                <input type="radio" name="kelamin" id="optionsRadios2" value="2">
                                Perempuan
                            </label>
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Kelas</label>
                        <select class="form-control" id="kelas" name="id_kelas" required>
                        </select>
                    </div>

                    <input type="hidden" class="form-control" name="mode" value="tambah">
                    <div class="form-group">
                        <button type="submit" class="btn btn-danger">Tambah</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $.get("<?php route("ambilSemuaKelas");?>"
            , function(raw){
                var data = $.parseJSON(raw);
                var err = data[0];
                var rawData = data[1];
                if(!err){
                    for(var i = 0; i < rawData.length; i++){
                        var tmpData = rawData[i];
                        $('#kelas').append("" +
                            "<option value=" + tmpData['id_kelas'] +">" +
                                tmpData['nama_kelas'] +
                            "</option>" +
                        "");
                    }
                }
        });
    });
</script>
