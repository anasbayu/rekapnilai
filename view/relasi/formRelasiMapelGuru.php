<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Relasi Mata Pelajaran & Guru
            <br>
            <small>Hubungan mata pelajaran dengan guru</small>
        </h1>
    </section>
    <section class="content">
        <?php
            error_reporting(0);
            if($_GET["err"]){?>
                <div class="callout callout-danger lead">
                    <p>Terjadi kesalahan, silahkan ulangi kembali</p>
                </div>
        <?php
            }
            if($_GET["sukses"]){?>
                <div class="callout callout-success lead">
                    <p>Berhasil Menambahkan Data Relasi</p>
                </div>
        <?php
            }
        ?>
        <div class="box box-danger">
            <div class="box-header with-border">
                <h3 class="box-title">Tambah Data Relasi</h3>
            </div>
            <div class="box-body">
                <form role="form" method="post" action="<?php route('tambahRelasiMapelGuru')?>">
                    <div class="form-group">
                        <label>Mata Pelajaran</label>
                        <select class="form-control" id="mapel" name="id_mapel" required>
                        </select>
                    </div>

                    <div class="form-group">
                        <label>Guru Pengajar</label>
                        <select class="form-control" multiple id="pengajar" name="id_guru[]" required>
                        </select>
                    </div>

                    <input type="hidden" class="form-control" name="mode" value="tambahRelasiMapelGuru">
                    <div class="form-group">
                        <button type="submit" class="btn btn-danger">Tambah</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $.get("<?php route("ambilSemuaMapel");?>"
            , function(raw){
                var data = $.parseJSON(raw);
                var err = data[0];
                var rawData = data[1];
                if(!err){
                    for(var i = 0; i < rawData.length; i++){
                        var tmpData = rawData[i];
                        $('#mapel').append("" +
                            "<option value=" + tmpData['id_mapel'] +">" +
                                tmpData['nama_mapel'] +
                            "</option>" +
                        "");
                    }
                }
        });

        $.get("<?php route("ambilSemuaGuru");?>"
            , function(raw){
                var data = $.parseJSON(raw);
                var err = data[0];
                var rawData = data[1];
                if(!err){
                    for(var i = 0; i < rawData.length; i++){
                        var tmpData = rawData[i];
                        $('#pengajar').append("" +
                            "<option value=" + tmpData['id_guru'] +">" +
                                tmpData['nama_guru'] +
                            "</option>" +
                        "");
                    }
                }
        });
    });
</script>
