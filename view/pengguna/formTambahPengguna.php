<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Data Pengguna
            <br>
            <small>Data master Pengguna</small>
        </h1>
    </section>
    <section class="content">
        <?php
            error_reporting(0);
            if($_GET["err"]){?>
                <div class="callout callout-danger lead">
                    <p>Terjadi kesalahan, silahkan ulangi kembali</p>
                </div>
        <?php
            }
            if($_GET["sukses"]){?>
                <div class="callout callout-success lead">
                    <p>Berhasil Menambahkan Data Pengguna</p>
                </div>
        <?php
            }
        ?>
        <div class="box box-danger">
            <div class="box-header with-border">
                <h3 class="box-title">Tambah Data Pengguna</h3>
            </div>
            <div class="box-body">
                <form role="form" method="post" action="<?php route('tambahPengguna')?>">
                    <div class="form-group">
                        <label>Username</label>
                        <input type="text" class="form-control" name="username" placeholder="Nama Pengguna" required>
                    </div>

                    <div class="form-group">
                        <label>Password</label>
                        <input type="password" class="form-control" name="password" placeholder="Password" required>
                    </div>

                    <div class="form-group">
                        <label>Jenis Pengguna</label>
                        <select class="form-control" id="jenis" name="id_jenis_pengguna" required>
                        </select>
                    </div>

                    <input type="hidden" class="form-control" name="mode" value="tambah">
                    <div class="form-group">
                        <button type="submit" class="btn btn-danger">Tambah</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $.get("<?php route("ambilJenisPengguna");?>"
            , function(raw){
                var data = $.parseJSON(raw);
                var err = data[0];
                var rawData = data[1];
                if(!err){
                    for(var i = 0; i < rawData.length; i++){
                        var tmpData = rawData[i];
                        $('#jenis').append("" +
                            "<option value=" + tmpData['id_jenis_pengguna'] +">" +
                                tmpData['nama_jenis_pengguna'] +
                            "</option>" +
                        "");
                    }
                }
        });
    });
</script>
