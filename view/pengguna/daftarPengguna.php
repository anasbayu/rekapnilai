<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Data Pengguna
            <br>
            <small>Data master Pengguna</small>
        </h1>
    </section>
    <section class="content">
        <?php
            error_reporting(0);
            if($_GET["err"]){?>
                <div class="callout callout-danger lead">
                    <p>Terjadi kesalahan, silahkan ulangi kembali</p>
                </div>
        <?php
            }
            if($_GET["sukses"]){?>
                <div class="callout callout-success lead">
                    <p>Berhasil Merubah Data Pengguna</p>
                </div>
        <?php
            }
        ?>
        <div class="box box-danger">
            <div class="box-header with-border">
                <h3 class="box-title">Kelola Data Pengguna</h3>
            </div>
            <div class="box-body">
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama Pengguna</th>
                            <th>Jenis Pengguna</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody id="tabel">
                        <script type="text/javascript">
                            $(document).ready(function(){
                                $.get("<?php route("ambilSemuaPengguna");?>"
                                    , function(raw){
                                        var data = $.parseJSON(raw);
                                        var err = data[0];
                                        var rawData = data[1];
                                        if(!err){
                                            for(var i = 0; i < rawData.length; i++){
                                                var tmpData = rawData[i];
                                                $('#tabel').append("" +
                                                    "<tr>" +
                                                        "<td>" + (i+1) +"</td>" +
                                                        "<td>" + tmpData['username'] +"</td>" +
                                                        "<td>" + tmpData['nama_jenis_pengguna'] +"</td>" +
                                                        "<td><a  href='#' class='btn btn-xs btn-info'><span class='fa fa-pencil'> </span></a> | <a class='btn btn-xs btn-danger' href='<?php route("hapusPengguna")?>" + tmpData['id_pengguna'] + "'><span class='fa fa-trash'> </span></a>" + "</td>" +
                                                    "</tr>" +
                                                "");
                                            }
                                        }
                                });
                            });
                        </script>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
</div>
