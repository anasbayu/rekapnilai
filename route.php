<?php
    $basePath = "/rekapNilai/";

    function route($jenis){
        global $basePath;

        switch ($jenis) {
            case 'login':
                echo $basePath . "controller/penggunaController.php";
                break;
            case 'logout':
                echo $basePath . "controller/penggunaController.php?mode=logout";
                break;
            case 'formLogin':
                header('location: ' . $basePath . 'view/login.php');
                break;
            case 'ErrformLogin':
                header('location: ' . $basePath . 'view/login.php?err=true');
                break;
            case 'dashboard':
                echo $basePath . "view/index.php?halaman=dashboard";
                break;
            case 'keDashboard':
                header('location: ' . $basePath . 'view/index.php?halaman=dashboard');
                break;

            // ============ UBAH PASS ===============
            case 'formUbahPassword':
                echo $basePath . "view/index.php?halaman=formUbahPassword";
                break;
            case 'ubahPassword':
                echo $basePath . "controller/penggunaController.php?mode=ubahPassword";
                break;
            case 'errFormUbahPassword':
                header('location: ' . $basePath . "view/index.php?halaman=formUbahPassword&err=true");
                break;
            case 'errFormUbahPasswordPassnya':
                header('location: ' . $basePath . "view/index.php?halaman=formUbahPassword&err=pass");
                break;
            case 'suksesFormUbahPassword':
                header('location: ' . $basePath . "view/index.php?halaman=formUbahPassword&sukses=true");
                break;


            // ============ GURU ==============
            case 'formTambahGuru':
                echo $basePath . "view/index.php?halaman=tambahGuru";
                break;
            case 'ambilSemuaGuru':
                echo $basePath . "controller/GuruController.php?mode=daftarGuru";
                break;
            case 'tambahGuru':
                echo $basePath . "controller/GuruController.php";
                break;
            case 'hapusGuru':
                echo $basePath . "controller/GuruController.php?mode=hapusGuru&id_guru=";
                break;
            case 'listGuru':
                echo $basePath . 'view/index.php?halaman=kelolaGuru';
                break;
            case 'suksesListGuru':
                header('location:' . $basePath . 'view/index.php?halaman=kelolaGuru&sukses=true');
                break;
            case 'errListGuru':
                header('location: ' . $basePath . 'view/index.php?halaman=kelolaGuru&err=true');
                break;
            case 'ErrformTambahGuru':
                header('location: ' . $basePath . 'view/index.php?halaman=tambahGuru&err=true');
                break;
            case 'suksesFormTambahGuru':
                header('location: ' . $basePath . 'view/index.php?halaman=tambahGuru&sukses=true');
                break;


            //================ MAPEL =================
            case 'formTambahMapel':
                echo $basePath . "view/index.php?halaman=tambahMapel";
                break;
            case 'ambilSemuaMapel':
                echo $basePath . "controller/MapelController.php?mode=daftarMapel";
                break;
            case 'tambahMapel':
                echo $basePath . "controller/MapelController.php";
                break;
            case 'hapusMapel':
                echo $basePath . "controller/MapelController.php?mode=hapusMapel&id_mapel=";
                break;
            case 'listMapel':
                echo $basePath . 'view/index.php?halaman=kelolaMapel';
                break;
            case 'suksesListMapel':
                header('location:' . $basePath . 'view/index.php?halaman=kelolaMapel&sukses=true');
                break;
            case 'errListMapel':
                header('location: ' . $basePath . 'view/index.php?halaman=kelolaMapel&err=true');
                break;
            case 'ErrformTambahMapel':
                header('location: ' . $basePath . 'view/index.php?halaman=tambahMapel&err=true');
                break;
            case 'suksesFormTambahMapel':
                header('location: ' . $basePath . 'view/index.php?halaman=tambahMapel&sukses=true');
                break;

            //================ KELAS =================
            case 'formTambahKelas':
                echo $basePath . "view/index.php?halaman=tambahKelas";
                break;
            case 'ambilSemuaKelas':
                echo $basePath . "controller/KelasController.php?mode=daftarKelas";
                break;
            case 'tambahKelas':
                echo $basePath . "controller/KelasController.php";
                break;
            case 'hapusKelas':
                echo $basePath . "controller/KelasController.php?mode=hapusKelas&id_kelas=";
                break;
            case 'listKelas':
                echo $basePath . 'view/index.php?halaman=kelolaKelas';
                break;
            case 'suksesListKelas':
                header('location:' . $basePath . 'view/index.php?halaman=kelolaKelas&sukses=true');
                break;
            case 'errListKelas':
                header('location: ' . $basePath . 'view/index.php?halaman=kelolaKelas&err=true');
                break;
            case 'ErrformTambahKelas':
                header('location: ' . $basePath . 'view/index.php?halaman=tambahKelas&err=true');
                break;
            case 'suksesFormTambahKelas':
                header('location: ' . $basePath . 'view/index.php?halaman=tambahKelas&sukses=true');
                break;

            //================ PENGGUNA =================
            case 'formTambahPengguna':
                echo $basePath . "view/index.php?halaman=tambahPengguna";
                break;
            case 'ambilSemuaPengguna':
                echo $basePath . "controller/PenggunaController.php?mode=daftarPengguna";
                break;
            case 'ambilJenisPengguna':
                echo $basePath . "controller/PenggunaController.php?mode=jenisPengguna";
                break;
            case 'tambahPengguna':
                echo $basePath . "controller/PenggunaController.php";
                break;
            case 'hapusPengguna':
                echo $basePath . "controller/PenggunaController.php?mode=hapusPengguna&id_pengguna=";
                break;
            case 'listPengguna':
                echo $basePath . 'view/index.php?halaman=kelolaPengguna';
                break;
            case 'suksesListPengguna':
                header('location:' . $basePath . 'view/index.php?halaman=kelolaPengguna&sukses=true');
                break;
            case 'errListPengguna':
                header('location: ' . $basePath . 'view/index.php?halaman=kelolaPengguna&err=true');
                break;
            case 'ErrformTambahPengguna':
                header('location: ' . $basePath . 'view/index.php?halaman=tambahPengguna&err=true');
                break;
            case 'suksesFormTambahPengguna':
                header('location: ' . $basePath . 'view/index.php?halaman=tambahPengguna&sukses=true');
                break;
            //================ TUGAS =================
            case 'formTambahTugas':
                echo $basePath . "view/index.php?halaman=tambahTugas";
                break;
            case 'ambilSemuaTugas':
                echo $basePath . "controller/tugasController.php?mode=daftarTugas";
                break;
            case 'tambahTugas':
                echo $basePath . "controller/tugasController.php";
                break;
            case 'hapusTugas':
                echo $basePath . "controller/tugasController.php?mode=hapusTugas&idTugas=";
                break;
            case 'listTugas':
                echo $basePath . 'view/index.php?halaman=kelolaTugas';
                break;
            case 'suksesListTugas':
                header('location:' . $basePath . 'view/index.php?halaman=kelolaTugas&sukses=true');
                break;
            case 'errListTugas':
                header('location: ' . $basePath . 'view/index.php?halaman=kelolaTugas&err=true');
                break;
            case 'suksesHapusTugas':
                header('location:' . $basePath . 'view/index.php?halaman=kelolaTugas&suksesHapus=true');
                break;
            case 'ErrformTambahTugas':
                header('location: ' . $basePath . 'view/index.php?halaman=tambahTugas&err=true');
                break;
            case 'suksesFormTambahTugas':
                header('location: ' . $basePath . 'view/index.php?halaman=tambahTugas&sukses=true');
                break;
            //===================ULANGAN HARIAN============
            case 'formTambahUH':
                echo $basePath . "view/index.php?halaman=tambahUH";
                break;
            case 'ambilSemuaUH':
                echo $basePath . "controller/UHController.php?mode=daftarUH";
                break;
            case 'tambahUH':
                echo $basePath . "controller/UHController.php";
                break;
            case 'hapusUH':
                echo $basePath . "controller/UHController.php?mode=hapusUH&idUH=";
                break;
            case 'listUH':
                echo $basePath . 'view/index.php?halaman=kelolaUH';
                break;
            case 'suksesListUH':
                header('location:' . $basePath . 'view/index.php?halaman=kelolaUH&sukses=true');
                break;
            case 'errListUH':
                header('location: ' . $basePath . 'view/index.php?halaman=kelolaUH&err=true');
                break;
            case 'suksesHapusUH':
                header('location:' . $basePath . 'view/index.php?halaman=kelolaUH&suksesHapus=true');
                break;
             case 'ErrformTambahUH':
                header('location: ' . $basePath . 'view/index.php?halaman=tambahTugas&err=true');
                break;
            case 'suksesFormTambahUH':
                header('location: ' . $basePath . 'view/index.php?halaman=tambahTugas&sukses=true');
                break;
            // ================NILAI TUGAS===============
            case 'formTambahNilaiTgs':
                echo $basePath . "view/index.php?halaman=tambahNilaiTgs";
                break;
            //================ SISWA =================
            case 'formTambahSiswa':
                echo $basePath . "view/index.php?halaman=tambahSiswa";
                break;
            case 'ambilSemuaSiswa':
                echo $basePath . "controller/SiswaController.php?mode=daftarSiswa";
                break;
            case 'tambahSiswa':
                echo $basePath . "controller/SiswaController.php";
                break;
            case 'hapusSiswa':
                echo $basePath . "controller/SiswaController.php?mode=hapusSiswa&id_siswa=";
                break;
            case 'listSiswa':
                echo $basePath . 'view/index.php?halaman=kelolaSiswa';
                break;
            case 'suksesListSiswa':
                header('location:' . $basePath . 'view/index.php?halaman=kelolaSiswa&sukses=true');
                break;
            case 'errListSiswa':
                header('location: ' . $basePath . 'view/index.php?halaman=kelolaSiswa&err=true');
                break;
            case 'ErrformTambahSiswa':
                header('location: ' . $basePath . 'view/index.php?halaman=tambahSiswa&err=true');
                break;
            case 'suksesFormTambahSiswa':
                header('location: ' . $basePath . 'view/index.php?halaman=tambahSiswa&sukses=true');
                break;

            // ========== RELASI ===============
            case 'formRelasiMapelGuru':
                echo $basePath . "view/index.php?halaman=formRelasiMapelGuru";
                break;
            case 'daftarRelasiMapelGuru':
                echo $basePath . "view/index.php?halaman=daftarRelasiMapelGuru";
                break;
            case 'tambahRelasiMapelGuru':
                echo $basePath . "controller/GuruController.php";
                break;
            case 'suksesFormTambahRelasiGuruMapel':
                header('location: ' . $basePath . 'view/index.php?halaman=formRelasiMapelGuru&sukses=true');
                break;
            case 'ambilSemuaRelasiMapelGuru':
                echo $basePath . "controller/MapelController.php?mode=ambilRelasiGuru";
                break;

            default:
                # code...
                break;
        }
    }

?>
