<?php
    require_once("../model/Mapel.php");
    require_once("../route.php");
    session_start();
    error_reporting(0);

    $mode = $_POST['mode'];

    if(!isset($mode)){
        $mode = $_GET['mode'];
    }

    switch ($mode) {
        case 'tambah':
            tambah();
            break;
        case 'daftarMapel':
            getAll();
            break;
        case 'hapusMapel':
            $id = $_GET['id_mapel'];
            hapus($id);
            break;
        case 'ambilRelasiGuru':
            ambilRelasiGuru();
            break;
        default:
            # code...
            break;
    }


    function tambah(){
        $nama = $_POST['nama'];

        $mapel = new Mapel();
        $err = $mapel->tambah($nama);

        // Jika berhasil tambah, ke form, kalau error ke form dengan err.
        if($err){
            route("ErrformTambahMapel");
        }else{
            route("suksesFormTambahMapel");
        }
    }


    function getAll(){
        $mapel = new Mapel();
        $data = $mapel->getAll();

        echo json_encode($data);
    }

    function hapus($id){
        $mapel = new Mapel();
        $berhasil = $mapel->hapus($id);

        if($berhasil){
            route("suksesListMapel");
        }else{
            route("errListMapel");
        }
    }

    function ambilRelasiGuru(){
        $mapel = new Mapel();
        $data = $mapel->getAllRelasiGuru();

        echo json_encode($data);
    }
?>
