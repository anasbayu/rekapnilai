<?php
    require_once("../model/Guru.php");
    require_once("../route.php");
    session_start();
    //error_reporting(0);

    $mode = $_POST['mode'];

    if(!isset($mode)){
        $mode = $_GET['mode'];
    }

    switch ($mode) {
        case 'tambah':
            tambah();
            break;
        case 'daftarGuru':
            getAll();
            break;
        case 'hapusGuru':
            $idGuru = $_GET['id_guru'];
            hapus($idGuru);
            break;
        case 'tambahRelasiMapelGuru':
            tambahRelasiMapelGuru();
            break;
        default:
            # code...
            break;
    }


    function tambah(){
        $nama = $_POST['nama'];
        $nip = $_POST['nip'];
        $jk = $_POST['kelamin'];

        $guru = new Guru();
        $err = $guru->tambah($nama, $nip, $jk);

        // Jika berhasil tambah, ke form, kalau error ke form dengan err.
        if($err){
            route("ErrformTambahGuru");
        }else{
            route("suksesFormTambahGuru");
        }
    }

    function getAll(){
        $guru = new Guru();
        $data = $guru->getAll();

        echo json_encode($data);
    }

    function hapus($idGuru){
        $guru = new Guru();
        $berhasil = $guru->hapus($idGuru);

        if($berhasil){
            route("suksesListGuru");
        }else{
            route("errListGuru");
        }
    }

    function tambahRelasiMapelGuru(){
        $idGuru = $_POST['id_guru'];    // Ini array.
        $idMapel = $_POST['id_mapel'];

        $guru = new Guru();
        foreach ($idGuru as $gurunya) {
            $a = $guru->tambahRelasiMapelGuru($gurunya, $idMapel);
        }

        // Err checkin blm bener.
        // if($err){
        //     route("ErrformTambahGuru");
        // }else{
        //     route("suksesFormTambahGuru");
        // }
        route("suksesFormTambahRelasiGuruMapel");
    }
?>
