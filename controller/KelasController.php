<?php
    require_once("../model/Kelas.php");
    require_once("../route.php");
    session_start();
    error_reporting(0);

    $mode = $_POST['mode'];

    if(!isset($mode)){
        $mode = $_GET['mode'];
    }

    switch ($mode) {
        case 'tambah':
            tambah();
            break;
        case 'daftarKelas':
            getAll();
            break;
        case 'hapusKelas':
            $id = $_GET['id_kelas'];
            hapus($id);
            break;
        default:
            # code...
            break;
    }


    function tambah(){
        $nama = $_POST['nama_kelas'];
        $idGuru = $_POST['id_guru'];

        $kelas = new Kelas();
        $err = $kelas->tambah($nama, $idGuru);

        // Jika berhasil tambah, ke form, kalau error ke form dengan err.
        if($err){
            route("ErrformTambahKelas");
        }else{
            route("suksesFormTambahKelas");
        }
    }


    function getAll(){
        $kelas = new Kelas();
        $data = $kelas->getAll();

        echo json_encode($data);
    }

    function hapus($id){
        $kelas = new Kelas();
        $berhasil = $kelas->hapus($id);

        if($berhasil){
            route("suksesListKelas");
        }else{
            route("errListKelas");
        }
    }
?>
