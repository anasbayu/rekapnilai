<?php
    require_once("../model/Pengguna.php");
    require_once("../route.php");
    session_start();
    error_reporting(0);

    $mode = $_POST['mode'];

    if(!isset($mode)){
        $mode = $_GET['mode'];
    }

    switch ($mode) {
        case 'tambah':
            tambah();
            break;
        case 'daftarPengguna':
            getAll();
            break;
        case 'jenisPengguna':
            getAllJenis();
            break;
        case 'hapusPengguna':
            $id = $_GET['id_pengguna'];
            hapus($id);
            break;
        case 'login':
            login();
            break;
        case 'logout':
            logout();
            break;
        case 'ubahPassword':
            ubahPassword();
            break;
        default:
            # code...
            break;
    }


    function tambah(){
        $nama = $_POST['username'];
        $pass = $_POST['password'];
        $idJenisPengguna = $_POST['id_jenis_pengguna'];

        $pengguna = new Pengguna();
        $err = $pengguna->tambah($nama, $pass, $idJenisPengguna);

        // Jika berhasil tambah, ke form, kalau error ke form dengan err.
        if($err){
            route("ErrformTambahPengguna");
        }else{
            route("suksesFormTambahPengguna");
        }
    }


    function getAll(){
        $pengguna = new Pengguna();
        $data = $pengguna->getAll();

        echo json_encode($data);
    }

    function getAllJenis(){
        $pengguna = new Pengguna();
        $data = $pengguna->getAllJenis();
        // var_dump($data);

        echo json_encode($data);
    }

    function hapus($id){
        $pengguna = new Pengguna();
        $berhasil = $pengguna->hapus($id);

        if($berhasil){
            route("suksesListPengguna");
        }else{
            route("errListPengguna");
        }
    }

    function login(){
        $username = $_POST['username'];
        $pass = $_POST['password'];

        $pengguna = new Pengguna();
        $data = $pengguna->login($username, $pass);
        $err = $data[0];

        // Jika berhasil login.
        if(!$err){
            $_SESSION['username'] = $data[1];
            $_SESSION['level'] = $data[2];
            route("keDashboard");
        }else{
            route("ErrformLogin");
        }
    }

    function logout(){
        session_destroy();
        route("formLogin");
    }

    function ubahPassword(){
        $lama = $_POST['pass_lama'];
        $baru = $_POST['pass_baru'];
        $konfirm = $_POST['pass_konfirm'];
        $username = $_SESSION['username'];

        // Kalau pass tidak sama, kembali ke form.
        if($konfirm != $baru){
            route('errFormUbahPasswordPassnya');
        }else{
            $pengguna = new Pengguna();
            $err = $pengguna->ubahPass($lama, $baru, $username);

            if($err){
                route("errFormUbahPassword");
            }else{
                route("suksesFormUbahPassword");
            }
        }
    }
?>
