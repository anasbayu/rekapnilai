<?php
    require_once("../model/Ulangan.php");
    require_once("../route.php");
    session_start();
    error_reporting(0);  

    $mode = $_POST['mode'];

    if(!isset($mode)){
        $mode = $_GET['mode'];
    }

    switch ($mode) {
        case 'tambah':
            tambah();
            // echo "tambah";
            break;
        case 'daftarUH':
            getAll();
            // echo "daftar";
            break;
        case 'hapusUH':
            $idUH = $_GET['idUH'];
            hapus($idUH);
            break;
        default:
            # code...
            break;
    }


    function tambah(){
        // echo "ini tambah ya";
        $dataulang = $_POST['ulangan'];

        $ulangan = new Ulangan();
        $err = $ulangan->tambah($dataulang);

        //Jika berhasil tambah, ke form, kalau error ke form dengan err.
        if($err){
            route("ErrformTambahUH");
        }else{
            route("suksesFormTambahUH");
        }
    }

    function getAll(){
        $ulangan = new Ulangan();
        $data = $ulangan->getAll();

        echo json_encode($data);
    }

    function hapus($idUH){
        $ulangan = new Ulangan();
        $berhasil = $ulangan->hapus($idUH);

        if($berhasil){
            route("suksesHapusUH");
        }else{
            route("errListUH");
        }
    }
?>
