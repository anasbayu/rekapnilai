<?php
    require_once("../model/Tugas.php");
    require_once("../route.php");
    session_start();
    error_reporting(0);

    $mode = $_POST['mode'];

    if(!isset($mode)){
        $mode = $_GET['mode'];
    }

    switch ($mode) {
        case 'tambah':
            tambah();
            break;
        case 'daftarTugas':
            getAll();
            break;
        case 'hapusTugas':
            $idTugas = $_GET['idTugas'];
            hapus($idTugas);
            break;
        default:
            # code...
            break;
    }


    function tambah(){
        // echo "ini tambah ya";
        $datatugas = $_POST['tugas'];

        $tugas = new Tugas();
        $err = $tugas->tambah($datatugas);

        //Jika berhasil tambah, ke form, kalau error ke form dengan err.
        if($err){
            route("ErrformTambahTugas");
        }else{
            route("suksesFormTambahTugas");
        }
    }

    function getAll(){
        $tugas = new Tugas();
        $data = $tugas->getAll();

        echo json_encode($data);
    }

    function hapus($idTugas){
        $tugas = new Tugas();
        $berhasil = $tugas->hapus($idTugas);

        if($berhasil){
            route("suksesHapusTugas");
        }else{
            route("errListTugas");
        }
    }
?>
