<?php
    require_once("../model/Siswa.php");
    require_once("../route.php");
    session_start();
    error_reporting(0);

    $mode = $_POST['mode'];

    if(!isset($mode)){
        $mode = $_GET['mode'];
    }

    switch ($mode) {
        case 'tambah':
            tambah();
            break;
        case 'daftarSiswa':
            getAll();
            break;
        case 'hapusSiswa':
            $id = $_GET['id_siswa'];
            hapus($id);
            break;
        default:
            # code...
            break;
    }


    function tambah(){
        $nama = $_POST['nama_siswa'];
        $nis = $_POST['nis'];
        $idKelas = $_POST['id_kelas'];
        $idKelamin = $_POST['kelamin'];

        $siswa = new Siswa();
        $err = $siswa->tambah($nama, $nis, $idKelas, $idKelamin);

        // Jika berhasil tambah, ke form, kalau error ke form dengan err.
        if($err){
            route("ErrformTambahSiswa");
        }else{
            route("suksesFormTambahSiswa");
        }
    }


    function getAll(){
        $siswa = new Siswa();
        $data = $siswa->getAll();

        echo json_encode($data);
    }

    function hapus($id){
        $siswa = new Siswa();
        $berhasil = $siswa->hapus($id);

        if($berhasil){
            route("suksesListSiswa");
        }else{
            route("errListSiswa");
        }
    }
?>
